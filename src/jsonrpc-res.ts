export default class RpcResponse {
    public code: number;
    public body: any;
    constructor(code: number, body: any) {
        this.code = code || 200;
        this.body = body;
    }
    public toObject(): object {
        return {
            code: this.code,
            body: this.body
        };
    }
}
