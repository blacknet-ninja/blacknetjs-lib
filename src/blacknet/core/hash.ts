class Hash {
    public Hash: Uint8Array;
    constructor(hash?: Uint8Array) {
        this.Hash = hash ? hash : new Uint8Array(32);
    }
    public Bytes(): Uint8Array {
        return this.Hash;
    }
    public Length(): number {
        return this.Hash.length;
    }
    public static Empty(): Hash {
        return new Hash();
    }
}

export default Hash;
