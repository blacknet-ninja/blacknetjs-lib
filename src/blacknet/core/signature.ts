class Signature {
    public Signature: Uint8Array;
    constructor(signature?: Uint8Array) {
        this.Signature = signature ? signature : new Uint8Array(64);
    }
    public Bytes(): Uint8Array {
        return this.Signature;
    }
    public Length(): number {
        return this.Signature.length;
    }
    public static Empty(): Signature {
        return new Signature();
    }
}

export default Signature;
