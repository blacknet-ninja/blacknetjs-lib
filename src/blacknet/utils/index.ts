const addr = require('../../lib/addr.js');
const hrp  = 'blacknet';

class Utils {
    public static EncodeVarInt = EncodeVarInt;
    public static NumberOfLeadingZeros = NumberOfLeadingZeros;
    public static DecodeVarInt = DecodeVarInt;
    public static HexToPublicKey = HexToPublicKey;
    public static PublicKeyToHex = PublicKeyToHex;
    public static PublicKey = PublicKey;
    public static PublicKeyToAccount = PublicKeyToAccount;
    public static ToHex = ToHex;
    public static ToByte = ToByte;
}
export default Utils;
// PublicKey account string to publickkey
export function PublicKey(account: string): Uint8Array {
    return Uint8Array.from(addr.decode(hrp, account));
}
// PublicKeyToAccount
export function PublicKeyToAccount(publicKey: Uint8Array): string {
    return addr.encode(hrp, Buffer.from(publicKey));
}
// PublicKeyToHex account string to publickkey hex
export function PublicKeyToHex(arr: Uint8Array): string {
    return Buffer.from(arr)
    .toString('hex')
    .toUpperCase();
}
// HexToPublicKey account string to publickkey hex
export function HexToPublicKey(str: string): Uint8Array {
    return Uint8Array.from(
        Buffer.from(str.toLowerCase(), 'hex')
    );
}
// ToHex
export function ToHex(arr: Uint8Array): string {
    return Buffer.from(arr)
    .toString('hex')
    .toUpperCase();
}
// ToByte
export function ToByte(str: string): Uint8Array {
    return Uint8Array.from(
        Buffer.from(str.toLowerCase(), 'hex')
    );
}
export function EncodeVarInt(value: number): Uint8Array {
    let shift = 31 - NumberOfLeadingZeros(value);
    shift -= shift % 7;
    const arr = [];
    while (shift !== 0) {
        arr.push(value >>> shift & 0x7F);
        shift -= 7;
    }
    arr.push(value & 0x7F | 0x80);
    return Uint8Array.from(arr);
}
export function DecodeVarInt(arr: Uint8Array): number {
    let ret: number;
    let offset = 0;
    let v: number;
    do {
        const buf = Buffer.from(
            arr.subarray(offset, offset + 1)
        );
        v = buf.readUInt8(0);
        ret = ret << 7 | (v & 0x7F);
        offset++;
    } while ((v & 0x80) === 0);
    return ret;
}
export function NumberOfLeadingZeros(i: number): number {
    if (i === 0) {
        return 32;
    }
    let n = 1;
    if (i >>> 16 === 0) {
        n += 16;
        i <<= 16;
    }
    if (i >>> 24 === 0) {
        n += 8;
        i <<= 8;
    }
    if (i >>> 28 === 0) {
        n += 4;
        i <<= 4;
    }
    if (i >>> 30 === 0) {
        n += 2;
        i <<= 2;
    }
    n -= i >>> 31;
    return n;
}
