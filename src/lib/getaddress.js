const nacl = require('./nacl.js');
const util = require('./util.js');
const bip39 = require('./bip39.js');
const addr = require('./addr.js');
const blacknet_seedWords = 12;
const blacknet_secretKeyLength = nacl.box_secretKeyLength;
const blacknet_bech32_hrp = "blacknet";
import {randomFillSync} from "crypto";

function strToArray(str) {
  return util.decodeUTF8(str);
}

function blacknet_sk_check_version(sk) {
  return (sk[0] & 0xF0) == 0x10;
}

function BlacknetMnemonic_sk(mnemonic) {
  let sk = nacl.hash(strToArray(mnemonic), blacknet_secretKeyLength);
  return blacknet_sk_check_version(sk) ? sk : null;
}

function BlacknetNnemonicKeypair(mnemonic) {
  let sk = BlacknetMnemonic_sk(mnemonic);
  if (!sk)
    return null;
  return nacl.sign_keyPair_fromSeed(sk);
}

function BlacknetMnemonic_check_version(mnemonic) {
  return BlacknetMnemonic_sk(mnemonic) != null;
}

function BlacknetMnemonic() {
  let seed = new Uint16Array(blacknet_seedWords);
  let mnemonic = "";

  while (true) {
    // windows
    // crypto.getRandomValues(seed);
    // node
    process.browser ? crypto.getRandomValues(seed) : randomFillSync(seed)
    for (let i = 0; i < blacknet_seedWords; i++) {
      mnemonic += bip39.english[seed[i] % 2048];
      if (i < blacknet_seedWords - 1) mnemonic += " ";
    }
    if (BlacknetMnemonic_check_version(mnemonic))
      break;
    mnemonic = "";
  }

  nacl.cleanup(seed);
  return mnemonic;
}

function blacknet_pk_account(pk) {
  return addr.encode(blacknet_bech32_hrp, Buffer.from(pk, 'hex'));
}

function BlacknetMnemonicCccount(m) {
  const kp = BlacknetNnemonicKeypair(m)
  return blacknet_pk_account(kp.publicKey)
}

// export default {
//   BlacknetMnemonic,
//   BlacknetNnemonicKeypair,
//   BlacknetMnemonicCccount,
//   blacknet_pk_account
// }

module.exports = {
  BlacknetMnemonic,
  BlacknetNnemonicKeypair,
  BlacknetMnemonicCccount
};