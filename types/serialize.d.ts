import JsonRPC from './jsonrpc';
import RpcResponse from './jsonrpc-res';
export default class {
    jsonRPC: JsonRPC;
    constructor(jsonRPC: JsonRPC);
    transfer(data: {
        fee?: number;
        amount: number;
        message?: string;
        to: string;
        from: string;
        encrypted?: number;
    }): Promise<RpcResponse>;
    lease(data: {
        fee: number;
        amount: number;
        to: string;
        from: string;
    }): Promise<RpcResponse>;
    cancelLease(data: {
        fee: number;
        amount: number;
        to: string;
        height: number;
        from: string;
    }): Promise<RpcResponse>;
    withdrawFromLease(data: {
        fee: number;
        withdraw: number;
        amount: number;
        to: string;
        height: number;
        from: string;
    }): Promise<RpcResponse>;
    getSeq(account: string): Promise<RpcResponse>;
}
