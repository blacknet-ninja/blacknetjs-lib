/// <reference types="node" />
import RpcResponse from './jsonrpc-res';
import serialize from './serialize';
export default class {
    endpoint: string;
    serialize: serialize;
    fetchBuiltin: (input?: Request | string, init?: RequestInit) => Promise<Response>;
    constructor(endpoint: string, args?: {
        fetch?: (input?: string | Request, init?: RequestInit) => Promise<Response>;
    });
    fetch(path: string, config?: object): Promise<Response>;
    post(path: string, body?: any, option?: object): Promise<RpcResponse>;
    get(path: string, option?: object): Promise<RpcResponse>;
    transfer(mnemonic: string | Buffer | Uint8Array, data: {
        fee?: number;
        amount: number;
        message?: string;
        to: string;
        from: string;
        encrypted?: number;
    }, options?: any): Promise<RpcResponse>;
    lease(mnemonic: string | Buffer | Uint8Array, data: {
        fee: number;
        amount: number;
        to: string;
        from: string;
    }, options?: any): Promise<RpcResponse>;
    getSeq(account: string, options?: any): Promise<RpcResponse>;
    private getFees;
    cancelLease(mnemonic: string | Buffer | Uint8Array, data: {
        fee: number;
        amount: number;
        to: string;
        height: number;
        from: string;
    }, options?: any): Promise<RpcResponse>;
    withdrawFromLease(mnemonic: string | Buffer | Uint8Array, data: {
        fee: number;
        withdraw: number;
        amount: number;
        to: string;
        height: number;
        from: string;
    }, options?: any): Promise<RpcResponse>;
    withdrawFromLeaseV2(mnemonic: string, data: {
        fee: number;
        withdraw: number;
        amount: number;
        to: string;
        height: number;
    }, options?: any): Promise<RpcResponse>;
}
