export default class RpcResponse {
    code: number;
    body: any;
    constructor(code: number, body: any);
    toObject(): object;
}
