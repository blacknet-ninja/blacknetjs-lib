/// <reference types="node" />
export declare function PublicKeyToAddress(publicKey: string | Buffer | Uint8Array): string | null;
export declare function Address(mnemonic: string | Buffer | Uint8Array): string | null;
export declare function Keypair(mnemonic: string, skipCheckVerion?: boolean): {
    publicKey: Uint8Array;
    secretKey: Uint8Array;
} | null;
export declare function PrivateToKeypair(privateKey: string | Buffer | Uint8Array): {
    publicKey: Uint8Array;
    secretKey: Uint8Array;
};
export declare function ToKeypair(mnemonic: string | Buffer | Uint8Array): {
    publicKey: Uint8Array;
    secretKey: Uint8Array;
};
export declare function Mnemonic(): string;
export declare function SignMessage(mnemonic: string | Buffer | Uint8Array, message: string): string;
export declare function VerifyMessage(account: string, signature: string, message: string): boolean;
export declare function Signature(mnemonic: string | Buffer | Uint8Array, serialized: string): string;
export declare function Decrypt(mnemonic: string | Buffer | Uint8Array, account: string, encrypt: string): string;
export declare function Encrypt(mnemonic: string | Buffer | Uint8Array, account: string, encrypt: string): string;
