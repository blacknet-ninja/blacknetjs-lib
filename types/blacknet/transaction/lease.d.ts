declare class Lease {
    Amount: number;
    To: string;
    constructor(amount: number, to: string);
    Serialize(): Uint8Array;
    static Derialize(arr: Uint8Array): Lease;
}
export default Lease;
