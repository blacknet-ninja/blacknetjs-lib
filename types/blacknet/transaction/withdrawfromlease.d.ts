declare class WithdrawFromLease {
    Withdraw: number;
    Amount: number;
    To: string;
    Height: number;
    constructor(withdraw: number, amount: number, to: string, height: number);
    Serialize(): Uint8Array;
    static Derialize(arr: Uint8Array): WithdrawFromLease;
}
export default WithdrawFromLease;
