import Message from '../core/message';
declare class Transfer {
    Amount: number;
    To: string;
    Message: Message;
    constructor(amount: number, to: string, message: Message);
    Serialize(): Uint8Array;
    static Derialize(arr: Uint8Array): Transfer;
}
export default Transfer;
