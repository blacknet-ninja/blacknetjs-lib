import cancellease from './cancellease';
import lease from './lease';
import transfer from './transfer';
import Signature from '../core/signature';
import Hash from '../core/hash';
export declare const CancelLease: typeof cancellease;
export declare const Lease: typeof lease;
export declare const Transfer: typeof transfer;
declare class Transaction {
    Signature: Signature;
    From: string;
    Seq: number;
    Hash: Hash;
    Fee: number;
    Type: number;
    Data: Uint8Array;
    constructor(from: string, seq: number, hash: Hash, fee: number, type: number, data: Uint8Array);
    Serialize(): Uint8Array;
    static Derialize(arr: Uint8Array): Transaction;
}
export default Transaction;
