declare class Hash {
    Hash: Uint8Array;
    constructor(hash?: Uint8Array);
    Bytes(): Uint8Array;
    Length(): number;
    static Empty(): Hash;
}
export default Hash;
