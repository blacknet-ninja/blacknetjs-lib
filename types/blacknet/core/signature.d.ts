declare class Signature {
    Signature: Uint8Array;
    constructor(signature?: Uint8Array);
    Bytes(): Uint8Array;
    Length(): number;
    static Empty(): Signature;
}
export default Signature;
