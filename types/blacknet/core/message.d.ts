declare class Message {
    Type: number;
    Message: string;
    constructor(type: number, message?: string);
    Serialize(): Uint8Array;
    static Derialize(arr: Uint8Array): Message;
    static Empty(): Message;
    static PLAIN: number;
    static ENCRYPTED: number;
}
export default Message;
