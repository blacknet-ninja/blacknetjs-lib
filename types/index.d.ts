import { SignMessage, VerifyMessage, Signature, Mnemonic, Address, Keypair, Decrypt, Encrypt, PublicKeyToAddress, PrivateToKeypair, ToKeypair } from './api';
import serialize from './serialize';
import JsonRPC from './jsonrpc';
import Option from './option';
declare class Blacknetjs {
    static Signature: typeof Signature;
    static VerifyMessage: typeof VerifyMessage;
    static SignMessage: typeof SignMessage;
    static Mnemonic: typeof Mnemonic;
    static Address: typeof Address;
    static Keypair: typeof Keypair;
    static Decrypt: typeof Decrypt;
    static Encrypt: typeof Encrypt;
    static PublicKeyToAddress: typeof PublicKeyToAddress;
    static PrivateToKeypair: typeof PrivateToKeypair;
    static ToKeypair: typeof ToKeypair;
    static Serialize: typeof serialize;
    static JSONRPC: typeof JsonRPC;
    static Config: typeof Option;
    serialize: serialize;
    jsonrpc: JsonRPC;
    config: Option;
    constructor(config?: Option);
}
export default Blacknetjs;
export declare const Serialize: typeof serialize;
export declare const JSONRPC: typeof JsonRPC;
export declare const Config: typeof Option;
