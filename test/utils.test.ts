import {EncodeVarInt, DecodeVarInt} from '../src/blacknet/utils';

test('EncodeVarInt And DecodeVarInt', async () => {
    const l = 1000;
	const res = EncodeVarInt(l);
    const v = DecodeVarInt(res);
    expect(v).toEqual(l)
})